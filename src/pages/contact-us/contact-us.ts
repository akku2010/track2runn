import { Component, OnInit } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController, ViewController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { IonPullUpFooterState } from 'ionic-pullup';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage implements OnInit {

  contact_data: any = {};
  contactusForm: FormGroup;
  submitAttempt: boolean;
  islogin: any;
  tickets: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  showDatePanel: boolean = false;
  footerState: IonPullUpFooterState;
  openNum: number = 0;
  closeNum: number = 0;
  inprogressNum: number = 0;
  tempTickets: any[] = [];
  selectedVehicle: any;
  device_id: any = [];
  device_name: any = [];
  portstemp: any;
  devices: any;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public api: ApiServiceProvider,
    public toastCtrl: ToastController,
    private modalCtrl: ModalController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    //this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeStart = moment().startOf('month').format();
    this.datetimeEnd = moment({ hours: 0 }).add(1, 'days').format();
    //this.datetimeEnd = moment().format();
    //new Date(a).toISOString();
    this.contactusForm = formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.email],
      mobno: ['', Validators.required],
      note: ['', Validators.required]
    });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter ContactUsPage');
  }

  ngOnInit() {
    this.viewTicket();
    this.getdevices();
  }

  getdevices() {

    var baseURLp = this.api.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    //this.api.startLoading().present();
    this.api.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        //this.api.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.api.stopLoading();
          console.log(err);
        });
  }

  call(num) {
    // this.callNumber.callNumber(num, true)
    //   .then(res => console.log('Launched dialer!', res))
    //   .catch(err => console.log('Error launching dialer', err));
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }
  contactUs() {
    this.submitAttempt = true;
    if (this.contactusForm.valid) {

      var payData = {
        "user": this.islogin._id,
        // "email": this.contactusForm.value.mail,
        "msg": this.contactusForm.value.note,
        "fileName": "sanskarvts.json",
        //"fileName":"sanskarvts.json",
        "DeviceIMEI": this.device_id,
        "VehicleNo": this.device_name,
        "mobile": this.contactusForm.value.mobno,
        "email": this.contactusForm.value.mail
      }
      this.api.startLoading().present();
      // this.api.contactusApi(this.contact_data)
      this.api.createTicketApi(payData)
        .subscribe(data => {
          this.api.stopLoading();
          console.log(data.message)
          if (data.message == 'saved response') {
            let toast = this.toastCtrl.create({
              // message: 'Your request has been submitted successfully. We will get back to you soon.',
              message: 'Your ticket has been submitted successfully. We will get back to you soon.',
              position: 'bottom',
              duration: 3000
            });
            toast.present();
            this.navCtrl.setRoot(ContactUsPage);
          } else {
            let toast = this.toastCtrl.create({
              message: 'Something went wrong. Please try after some time.',
              position: 'bottom',
              duration: 3000
            });

            toast.onDidDismiss(() => {
              console.log('Dismissed toast');
              this.navCtrl.setRoot(ContactUsPage);
            });

            toast.present();
          }

        },
          error => {
            this.api.stopLoading();
            console.log(error);
          });
    }
  }

  getSummaarydevice(selectedVehicle) {
    this.device_id = [];
    this.device_name = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.device_id.push(selectedVehicle[t].Device_ID)
          this.device_name.push(selectedVehicle[t].Device_Name)
        }
      } else {
        this.device_id.push(selectedVehicle[0].Device_ID)
        this.device_name.push(selectedVehicle[0].Device_Name)
      }
    } else return;
    console.log("selectedVehicle=> ", this.device_id)
  }

  addTicket() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  viewTicket() {
    var _bUrl = this.api.mainUrl + "customer_support/getCustomerQuery";
    var payload = {
      "draw": 1,
      "columns": [
        {
          "data": "_id"
        },
        {
          "data": "superAdmin"
        },
        {
          "data": "dealer"
        },
        {
          "data": "ticketId"
        },
        {
          "data": "message"
        },
        {
          "data": "assigned_to"
        },
        {
          "data": "assigned_to.first_name"
        },
        {
          "data": "assigned_to.last_name"
        },
        {
          "data": "support_status"
        },
        {
          "data": "posted_on"
        },
        {
          "data": "posted_by"
        },
        {
          "data": "posted_by.first_name"
        },
        {
          "data": "posted_by.last_name"
        },
        {
          "data": "posted_by.phone"
        },
        {
          "data": "posted_by.email"
        },
        {
          "data": "role"
        },
        {
          "data": "user"
        }
      ],
      "order": [
        {
          "column": 3,
          "dir": "desc",

        }
      ],
      "start": 0,
      "length": 50,
      "search": {
        "value": "",
        "regex": false
      },
      "op": {},
      "select": [],
      "find": {
        "posted_by": this.islogin._id,
        "posted_on": {
          "$gte": {
            "_eval": "date",
            "value": new Date(this.datetimeStart).toISOString()
          },
          "$lte": {
            "_eval": "date",
            "value": new Date(this.datetimeEnd).toISOString()
          }
        }
      }
    }
    this.tickets = [];
    this.openNum = 0;
    this.closeNum = 0;
    this.inprogressNum = 0;
    this.api.startLoading().present();
    this.api.urlpasseswithdata(_bUrl, payload)
      .subscribe(data => {
        this.api.stopLoading();
        console.log("tickets: ", data)
        this.tickets = data.data;
        this.tempTickets = data.data;
        for (var i = 0; i < data.data.length; i++) {
          if (data.data[i].support_status == 'OPEN') {
            this.openNum += 1;
          } else if (data.data[i].support_status == 'CLOSE') {
            this.closeNum += 1;
          } else if (data.data[i].support_status == 'IN PROGRESS') {
            this.inprogressNum += 1;
          }
        }
      },
        err => {
          this.api.stopLoading();
          console.log("getting err while getting data: ", err)
        })
  }

  loadContent(key) {
    if (key === 'PROGRESS') {
      this.tickets = [];
      for (var i = 0; i < this.tempTickets.length; i++) {
        if (this.tempTickets[i].support_status === 'IN PROGRESS') {
          this.tickets.push(this.tempTickets[i]);
        }
      }
    } else if (key === 'OPEN') {
      this.tickets = [];
      for (var r = 0; r < this.tempTickets.length; r++) {
        if (this.tempTickets[r].support_status === 'OPEN') {
          this.tickets.push(this.tempTickets[r]);
        }
      }
    } else if (key === 'CLOSE') {
      this.tickets = [];
      for (var v = 0; v < this.tempTickets.length; v++) {
        if (this.tempTickets[v].support_status === 'CLOSE') {
          this.tickets.push(this.tempTickets[v]);
        }
      }
    }
  }

  onClickChat() {
    let customTModal = this.modalCtrl.create(ShowSupportContactsModal);
    customTModal.present();

    customTModal.onDidDismiss((param) => {
    })
  }

}

@Component({
  selector: 'page-show-support-contacts',
  templateUrl: './show-support-contacts.html'
})

export class ShowSupportContactsModal {
  islogin: any = {};
  supportArray: any = [];
  serviceArray: any = [];

  constructor(private viewCtrl: ViewController, private apiCall: ApiServiceProvider,private callNumber: CallNumber) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log("check lang key: ", resp)
        if (resp.Service != undefined) {
          if (resp.Service.length > 0) {
            this.serviceArray = resp.Service;
          }
        }
        if (resp.Support != undefined) {
          if (resp.Support.length > 0) {
            this.supportArray = resp.Support;
          }
        }
      },
        err => {
          console.log(err);
          this.apiCall.stopLoading();
        });
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  call_num(number) {
    this.callNumber.callNumber(number.toString(), true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
}
