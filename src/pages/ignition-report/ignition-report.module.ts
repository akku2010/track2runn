import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IgnitionReportPage } from './ignition-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    IgnitionReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(IgnitionReportPage),
    SelectSearchableModule,
    TranslateModule.forChild(),
    ComponentsModule
  ],
  exports: [
    OnCreate
  ]
})
export class IgnitionReportPageModule {}
