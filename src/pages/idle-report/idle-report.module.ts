import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdleReportPage } from './idle-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    IdleReportPage
  ],
  imports: [
    IonicPageModule.forChild(IdleReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  // exports: [
  //   OnCreate
  // ],
})
export class IdleReportPageModule {}
