import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpiredPage } from './expired';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ExpiredPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpiredPage),
    TranslateModule.forChild()
  ],
  providers: [
    //  {provide: String, useValue: "SoapService"}
    // SoapService
  ],
})
export class ExpiredPageModule {}
